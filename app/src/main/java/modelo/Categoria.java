package modelo;

public class Categoria {
    private String categoria, descripcion;
    private Integer id;

    public Categoria(String categoria, String descripcion, Integer id) {
        this.categoria = categoria;
        this.descripcion = descripcion;
        this.id = id;
    }

    public Categoria(String categoria, String descripcion) {
        this.categoria = categoria;
        this.descripcion = descripcion;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
