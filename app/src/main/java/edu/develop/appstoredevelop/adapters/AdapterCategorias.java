package edu.develop.appstoredevelop.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import edu.develop.appstoredevelop.R;
import modelo.Categoria;

public class AdapterCategorias extends RecyclerView.Adapter<AdapterCategorias.CochesviewHolder> {

    List<Categoria> categorias;

    public AdapterCategorias(List<Categoria> categorias) {
        this.categorias = categorias;
    }

    @NonNull
    @Override
    public CochesviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row, parent, false);
        CochesviewHolder holder = new CochesviewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull CochesviewHolder holder, int position) {
        Categoria categoria = categorias.get(position);
        holder.txtcategoria.setText(categoria.getCategoria());
        holder.txtdescripcion.setText(categoria.getDescripcion());
    }

    @Override
    public int getItemCount() {
        return categorias.size();
    }

    public class CochesviewHolder extends RecyclerView.ViewHolder{
        TextView txtcategoria, txtdescripcion;
        public CochesviewHolder(@NonNull View itemView) {
            super(itemView);
            txtcategoria = (TextView) itemView.findViewById(R.id.txt_categoria);
            txtdescripcion = (TextView) itemView.findViewById(R.id.txt_descripciones);
        }
    }

}
