package edu.develop.appstoredevelop.adapters;

import android.os.Bundle;
import android.widget.Adapter;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.helper.widget.Carousel;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.core.DatabaseConfig;

import java.util.ArrayList;
import java.util.List;

import edu.develop.appstoredevelop.R;
import modelo.Categoria;

public class AdapterCategoriaActivity extends AppCompatActivity {

    RecyclerView rv;
    Adapter adapter;
    List<Categoria> categorias;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.adapter_categoria);

        rv = (RecyclerView) findViewById(R.id.recicle);
        rv.setLayoutManager(new LinearLayoutManager(this));

        categorias = new ArrayList<>();

        FirebaseDatabase database = FirebaseDatabase.getInstance();

        adapter = (Adapter) new AdapterCategorias(categorias);

        rv.setAdapter((RecyclerView.Adapter) adapter);

        database.getReference().getRoot().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                categorias.removeAll(categorias);
                for(DataSnapshot dataSnapshot : snapshot.getChildren()){
                    Categoria categoria = snapshot.getValue(Categoria.class);
                    categorias.add(categoria);
                }
                ((RecyclerView.Adapter<?>) adapter).notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


    }
}
