package edu.develop.appstoredevelop;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginAdminActivity extends AppCompatActivity {

    private EditText txt_email,txt_password;
    private Button btn_ingresar,btn_registrar;

    private FirebaseAuth auth;

    private String email = "";
    private String password = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_admin);

        auth = FirebaseAuth.getInstance();
        txt_email = findViewById(R.id.txt_email_admin);
        txt_password = findViewById(R.id.txt_password_admin);
        btn_ingresar = findViewById(R.id.btn_ingresar_admin);
        btn_registrar = findViewById(R.id.btn_registrar_admin);

        btn_registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginAdminActivity.this, RegisterAdminActivity.class);
                startActivity(intent);
            }
        });
        btn_ingresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                email = txt_email.getText().toString();
                password = txt_password.getText().toString();

                if(!email.isEmpty() && !password.isEmpty()){
                    loginAdmin();
                }else{
                    Toast.makeText(LoginAdminActivity.this, "Complete todos los campos", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void loginAdmin() {
        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    startActivity(new Intent(LoginAdminActivity.this, NavigationActivity.class));
                    finish();
                }else{
                    Toast.makeText(LoginAdminActivity.this, "No se puedo iniciar sesion \n Porfavor verifique lo datos \n Intente mas tarde", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}