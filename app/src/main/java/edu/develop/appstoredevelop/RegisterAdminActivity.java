package edu.develop.appstoredevelop;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class RegisterAdminActivity extends AppCompatActivity {

    private EditText txt_nombres,txt_direccion, txt_ciudad, txt_email, txt_password;
    private Button btn_registrar_admin;
    private DatabaseReference databaseReference;
    private FirebaseAuth auth;
    private String email ="";
    private String password ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_admin);

        auth = FirebaseAuth.getInstance();

        txt_nombres = findViewById(R.id.txt_nombres_admin);
        txt_direccion = findViewById(R.id.txt_direccion_admin);
        txt_ciudad = findViewById(R.id.txt_ciudad_admin);
        txt_email = findViewById(R.id.txt_email_admin);
        txt_password = findViewById(R.id.txt_password_admin);

        btn_registrar_admin = findViewById(R.id.btn_ingresar_admin);

        databaseReference = FirebaseDatabase.getInstance().getReference();

        btn_registrar_admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                email= txt_email.getText().toString();
                password= txt_password.getText().toString();
                auth.createUserWithEmailAndPassword(email,password).addOnCompleteListener((task)->{
                    if (task.isSuccessful()){
                        Map<String,Object> userMap = new HashMap<>();
                        userMap.put("nombres", txt_nombres.getText().toString());
                        userMap.put("direccion", txt_direccion.getText().toString());
                        userMap.put("ciudad", txt_ciudad.getText().toString());
                        userMap.put("email", txt_email.getText().toString());
                        userMap.put("password", txt_password.getText().toString());

                        databaseReference.child("Usuarios").child("Admins").push().setValue(userMap);

                        Intent intent = new Intent(RegisterAdminActivity.this, LoginAdminActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
            }
        });

    }
}