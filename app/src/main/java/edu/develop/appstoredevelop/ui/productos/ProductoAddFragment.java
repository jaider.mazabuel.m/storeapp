package edu.develop.appstoredevelop.ui.productos;

import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

import edu.develop.appstoredevelop.R;
import edu.develop.appstoredevelop.databinding.FragmentProductoAddBinding;
import edu.develop.appstoredevelop.databinding.FragmentProductosBinding;

public class ProductoAddFragment extends Fragment {

    private DatabaseReference databaseReference;
    private ProductoAddViewModel productoAddViewModel;
    private FragmentProductoAddBinding binding;
    private EditText txt_nombre, txt_descripcion, txt_valor, txt_stock;
    private Button btn_registrar_producto;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        productoAddViewModel = new ViewModelProvider(this).get(ProductoAddViewModel.class);

        binding = FragmentProductoAddBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        txt_nombre = root.findViewById(R.id.setup_nombre_proc);
        txt_descripcion = root.findViewById(R.id.setup_descripcion);
        txt_valor = root.findViewById(R.id.setup_valor);
        txt_stock = root.findViewById(R.id.setup_stock);

        btn_registrar_producto = root.findViewById(R.id.setup_boton);
        btn_registrar_producto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Map<String,Object> userMap = new HashMap<>();
                userMap.put("nombre", txt_nombre.getText().toString());
                userMap.put("descripcion", txt_descripcion.getText().toString());
                userMap.put("valor", txt_valor.getText().toString());
                userMap.put("stock", txt_stock.getText().toString());

                databaseReference.child("Categorias").child("Productos").push().setValue(userMap);

                // Crea el nuevo fragmento y la transacción.
                Fragment nuevoFragmento = new ProductosFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.nav_host_fragment_activity_navigation, nuevoFragmento);
                transaction.addToBackStack(null);
//data
                // Commit a la transacción
                transaction.commit();
            }
        });

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}