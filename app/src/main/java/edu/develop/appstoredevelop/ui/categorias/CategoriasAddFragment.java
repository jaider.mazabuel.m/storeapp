package edu.develop.appstoredevelop.ui.categorias;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.auth.FirebaseAuth;


import java.util.HashMap;
import java.util.Map;

import edu.develop.appstoredevelop.R;
import edu.develop.appstoredevelop.databinding.FragmentCategoriasAddBinding;
import edu.develop.appstoredevelop.databinding.FragmentProductoAddBinding;
import edu.develop.appstoredevelop.ui.productos.ProductoAddViewModel;
import edu.develop.appstoredevelop.ui.productos.ProductosFragment;

public class CategoriasAddFragment extends Fragment {

    private CategoriasAddViewModel categoriasAddViewModel;
    private FragmentCategoriasAddBinding binding;
    private EditText edit_nombre, edit_descripcion;
    private Button btn_addCategoria;
    private DatabaseReference databaseReference;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        categoriasAddViewModel = new ViewModelProvider(this).get(CategoriasAddViewModel.class);

        binding = FragmentCategoriasAddBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        databaseReference = FirebaseDatabase.getInstance().getReference();

        edit_nombre = root.findViewById(R.id.edit_nombre_categoria);
        edit_descripcion = root.findViewById(R.id.edit_descripcion_categorias);

        btn_addCategoria = root.findViewById(R.id.btn_addCategoria);

        btn_addCategoria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!edit_nombre.getText().toString().isEmpty() && !edit_descripcion.getText().toString().isEmpty()){
                    Map<String, Object> CateMap = new HashMap<>();
                    CateMap.put("nombre", edit_nombre.getText().toString());
                    CateMap.put("descripcion",edit_descripcion.getText().toString());
                    databaseReference.child("Categorias").push().setValue(CateMap);

                    // Crea el nuevo fragmento y la transacción.
                    Fragment nuevoFragmento = new CategoriasFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.nav_host_fragment_activity_navigation, nuevoFragmento);
                    transaction.addToBackStack(null);
//data
                    // Commit a la transacción
                    transaction.commit();
                }else {
                    Toast.makeText(getContext(), "Falta información", Toast.LENGTH_SHORT).show();
                }
            }
        });


        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}