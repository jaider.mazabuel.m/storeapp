package edu.develop.appstoredevelop.ui.categorias;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import edu.develop.appstoredevelop.R;
import edu.develop.appstoredevelop.databinding.FragmentCategoriasAddBinding;
import modelo.Categoria;

public class CategoriaDeleteFragment extends Fragment {

    private CategoriasAddViewModel categoriasAddViewModel;
    private FragmentCategoriasAddBinding binding;
    private EditText edit_eliminarcategoria;
    private Button btn_deleteCategoria;
    private DatabaseReference databaseReference;

    private CategoriaDeleteViewModel mViewModel;

    public static CategoriaDeleteFragment newInstance() {
        return new CategoriaDeleteFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        categoriasAddViewModel = new ViewModelProvider(this).get(CategoriasAddViewModel.class);

        binding = FragmentCategoriasAddBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        databaseReference = FirebaseDatabase.getInstance().getReference();

        edit_eliminarcategoria = root.findViewById(R.id.edit_eliminar_categoria);
        btn_deleteCategoria = root.findViewById(R.id.btn_deleteCategoria);

        btn_deleteCategoria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!edit_eliminarcategoria.getText().toString().isEmpty()){
                    String eliminar = edit_eliminarcategoria.getText().toString();
                    databaseReference.child("Categoria").child(eliminar).removeValue();

                    Fragment nuevoFragmento = new CategoriasFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.nav_host_fragment_activity_navigation, nuevoFragmento);
                    transaction.addToBackStack(null);
                    // Commit a la transacción
                    transaction.commit();
                }
            }
        });
        return root;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(CategoriaDeleteViewModel.class);
        // TODO: Use the ViewModel
    }

}