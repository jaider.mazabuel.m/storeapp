package edu.develop.appstoredevelop.ui.productos;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ProductoAddViewModel extends ViewModel {
    private MutableLiveData<String> mText;

    public ProductoAddViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is dashboard fragment");
    }
    //data
    public LiveData<String> getText() {
        return mText;
    }
}