package edu.develop.appstoredevelop.ui.categorias;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class CategoriaDeleteViewModel extends ViewModel {
    private MutableLiveData<String> mText;
    //data
    public CategoriaDeleteViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is notifications fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}