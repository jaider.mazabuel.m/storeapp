package edu.develop.appstoredevelop.ui.ventas;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import edu.develop.appstoredevelop.databinding.FragmentVentasBinding;

public class VentasFragment extends Fragment {

    private VentasViewModel ventasViewModel;
    private FragmentVentasBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        ventasViewModel =
                new ViewModelProvider(this).get(VentasViewModel.class);

        binding = FragmentVentasBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}